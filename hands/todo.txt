* owner   module      (notes)

q Phillip defs.qc     (not merged, moved to hackdefs.qc)
q Phillip world.qc    (plus bodyque.qc)
q Chuck   combat.qc
q Chuck   weapons.qc  (plus cheats.qc)
q Ben     ai.qc       (was incorrectly called monsters.qc)
q Ben     fight.qc    (was incorrectly called monsters.qc)
q Ben     player.qc
q Ulrich  client.qc   (plus client_*.qc)
q Ulrich  triggers.qc (plus trigger_*.qc)
q Chuck   items.qc    (plus item_*.qc)
q Chuck   misc.qc     (plus func_*.qc, light.qc, misc_*.qc, traps.qc)

* status flags:

- == not merged
m == merged, needs QW merge double-checking
q == double-checked for QW merge
