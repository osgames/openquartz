/* Copyright (C) 1996-1997  Id Software, Inc.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

	See file, 'COPYING', for details.
*/

/*
// "Power-up" items or "Artifacts"
// name, model, sound, and even effects can be customized
class item_artifact
{
public:

	// generic artifact with no default effects
	void item_artifact();
	
	// standard artifacts have 30 seconds of one effect by default
	void item_artifact_invulnerability(); // 30 seconds of invulnerability
	void item_artifact_envirosuit();      // 30 seconds of burn/drown protection
	void item_artifact_invisibility();    // 30 seconds of invisibility
	void item_artifact_super_damage();    // 30 seconds of quad damage

	// but the map designer can change the values for individual items
	float invincible_finished;   // duration of invulnerability
	float radsuit_finished;      // duration of burn/drown protection
	float invisible_finished;    // duration of invisibility
	float super_damage_finished; // duration of quad damage

	int    skin;    // show this skin or animated skin group
	int    frame;   // show this animation frame or frame group
	string model;   // the .mdl, .bsp, or .spr to show as the item
	string netname; // name printed when you pick it up the item
	string noise;   // the sound played when you pick up the item
	
	vector mangle;  // use this for full control of pitch, yaw, and roll angles
	vector angle;   // (in editor only) a simpler way to set the model's angles
	                // if angle is 0..360 the item will face that direction
	                // if angle is -1 the item will face up
	                // if angle is -2 the item will face down
private:

	vector angles;  // (automatically determined) the orientation of the item
	float  items;   // (automatically determined) status bar graphics flags
	
	// the work-horse initialization function, precaches sounds, sets model, etc.
	void artifact_init (string snd, string mdl, string name,
		float vul, float env, float vis, float sup);
		
	// apply effects to player who gets the artifact and set respawn time
	void artifact_touch();
};
*/


void() artifact_touch =
{
	if (other.classname != "player" || other.health <= 0) return;

	sprint (other, PRINT_LOW, "You got the ");
	sprint (other, PRINT_LOW, self.netname);
	sprint (other, PRINT_LOW, "\n");

	if (deathmatch)
	{
		self.mdl = self.model;
		self.think = SUB_regen;
		self.nextthink = time;
		self.nextthink = self.nextthink + (10 * self.invincible_finished);
		self.nextthink = self.nextthink + (10 * self.invisible_finished);
		self.nextthink = self.nextthink + (2  * self.radsuit_finished);
		self.nextthink = self.nextthink + (2  * self.super_damage_finished);
	}	
	self.solid = SOLID_NOT;
	self.model = string_null;

	if ((other.invincible_finished   < time + self.invincible_finished)   && self.invincible_finished)
		{other.invincible_finished   = time + self.invincible_finished;   other.invincible_time = 1;}
	if ((other.radsuit_finished      < time + self.radsuit_finished)      && self.radsuit_finished)
		{other.radsuit_finished      = time + self.radsuit_finished;      other.rad_time        = 1;}
	if ((other.invisible_finished    < time + self.invisible_finished)    && self.invisible_finished)
		{other.invisible_finished    = time + self.invisible_finished;    other.invisible_time  = 1;}
	if ((other.super_damage_finished < time + self.super_damage_finished) && self.super_damage_finished)
		{other.super_damage_finished = time + self.super_damage_finished; other.super_time      = 1;}

	other.items = other.items | self.items;
	sound (other, CHAN_VOICE, self.noise, 1, ATTN_NORM);
	stuffcmd (other, "bf\n");
	activator = other;
	SUB_UseTargets();
};


void (string snd, string modl, string name,
	float vul, float env, float vis, float sup) artifact_init =
{
	if (!self.model)   self.model   = modl;
	if (!self.noise)   self.noise   = snd;
	if (!self.netname) self.netname = name;

	if (!self.invincible_finished)   self.invincible_finished   = vul;
	if (!self.radsuit_finished)      self.radsuit_finished      = env;
	if (!self.invisible_finished)    self.invisible_finished    = vis;
	if (!self.super_damage_finished) self.super_damage_finished = sup;

	if (self.invincible_finished)
	{
// disable invulnerability flag because the "666" armor value
// doesn't go with our default invulnerability model (an ankh)
//		self.items = self.items | IT_INVULNERABILITY;
		precache_sound ("items/protect2.wav");
		precache_sound ("items/protect3.wav");
#ifdef QUAKEWORLD
		self.effects = self.effects | EF_RED;
#endif /* QUAKEWORLD */
	}
	if (self.radsuit_finished)
	{
		self.items = self.items | IT_SUIT;
		precache_sound ("items/suit2.wav");
	}
	if (self.invisible_finished)
	{
		self.items = self.items | IT_INVISIBILITY;
		precache_sound ("items/inv2.wav");
		precache_sound ("items/inv3.wav");
	}
	if (self.super_damage_finished)
	{
		self.items = self.items | IT_QUAD;
		precache_sound ("items/damage2.wav");
		precache_sound ("items/damage3.wav");
#ifdef QUAKEWORLD
		self.effects = self.effects | EF_BLUE;
#endif /* QUAKEWORLD */
	}

	precache_sound (self.noise);
	precache_model (self.model);
	setmodel (self, self.model);
	setsize (self, '-16 -16 -24', '16 16 32'); // change to allow bmodels?
	
	if (self.angles_y == -1) self.angles = '90 0 0';
	if (self.angles_y == -2) self.angles = '-90 0 0';
	if (self.mangle)         self.angles = self.mangle;

	self.touch = artifact_touch;
	StartItem ();
};

void() item_artifact =
	{artifact_init ("items/protect.wav", "progs/s_globe.spr",  "Artifact",         0,0,0,0);};
void() item_artifact_invulnerability =
	{artifact_init ("items/protect.wav", "progs/invulner.mdl", "Protective Ankh", 30,0,0,0);};
void() item_artifact_envirosuit =
	{artifact_init ("items/suit.wav",    "progs/suit.mdl",     "Biosuit",         0,30,0,0);};
void() item_artifact_invisibility =
	{artifact_init ("items/inv1.wav",    "progs/invisibl.mdl", "Eye of Shadows",  0,0,30,0);};
void() item_artifact_super_damage =
	{artifact_init ("items/damage.wav",  "progs/quaddama.mdl", "Quad Damage",     0,0,0,30);};
