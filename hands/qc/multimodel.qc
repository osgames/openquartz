float modelindex_eyes, modelindex_player;/*  Copyright (C) 2000 - 2001 Seth Galbraith

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

    See file, 'COPYING', for details.
*/

float modelindex_eyes, modelindex_player;

float   multimodel_classes; // total number of classes for random classs selection
.float  multimodel_skins;   // number of skins in this multimodel class
.float  multimodel_size;    // -1 = tiny, 0 = normal size, 1 = giant sized
.float  multimodel_tilt;    // 0 = upright, 1 = erect, 2 = sprawler
.entity multimodel_class;   // a player-character or NPC's multimodel class entity

void(vector org) spawn_tfog;


void() multimodel_precache =
{
	// Called at the end of worldspawn()

	// precache default player models
    // registered = (100 * default skins) + default models
	local float classes, skins;
	skins   = floor (0.01 * cvar ("registered"));
	classes = cvar ("registered") - 100 * skins;
	while (classes > 0)
	{
		local entity e;
		e = spawn ();

		e.mins             = VEC_HULL_MIN;
		e.maxs             = VEC_HULL_MAX;
		e.style            = classes;
		e.classname        = "multimodel_class";
		e.multimodel_skins = skins;

		if (classes ==  1) e.model = "progs/player1.mdl";
		if (classes ==  2) e.model = "progs/player2.mdl";
		if (classes ==  3) e.model = "progs/player3.mdl";
		if (classes ==  4) e.model = "progs/player4.mdl";
		if (classes ==  5) e.model = "progs/player5.mdl";
		if (classes ==  6) e.model = "progs/player6.mdl";
		if (classes ==  7) e.model = "progs/player7.mdl";
		if (classes ==  8) e.model = "progs/player8.mdl";
		if (classes ==  9) e.model = "progs/player9.mdl";
		if (classes == 10) e.model = "progs/player10.mdl";
		if (classes == 11) e.model = "progs/player11.mdl";
		if (classes == 12) e.model = "progs/player12.mdl";
		if (classes == 13) e.model = "progs/player13.mdl";
		if (classes == 14) e.model = "progs/player14.mdl";
		if (classes == 15) e.model = "progs/player15.mdl";
		if (classes == 16) e.model = "progs/player16.mdl";
		if (classes == 17) e.model = "progs/player17.mdl";
		if (classes == 18) e.model = "progs/player18.mdl";
		if (classes == 19) e.model = "progs/player19.mdl";
		if (classes == 20) e.model = "progs/player20.mdl";

		precache_model (e.model);
		classes = classes - 1;
		multimodel_classes = multimodel_classes + 1;
	}
};

void (entity e) multimodel_setModel =
{
	setmodel (e, e.multimodel_class.model);
	//calling setsize here has funky effects on players (like null size)
	//setsize  (e, e.multimodel_class.mins, e.multimodel_class.maxs);
	e.skin = e.multimodel_class.skin;
};

void() multimodel_randomClass =
{
	// randomly select a multimodel class
	local float r;
	r = 1 + floor (multimodel_classes * random());
	if (r > multimodel_classes) r = 1;
	self.multimodel_class = world;
	while (r > 0)
	{
		self.multimodel_class = find (self.multimodel_class, classname, "multimodel_class");
		r = r - 1;
	}
	multimodel_setModel (self);
	setsize (self, self.multimodel_class.mins, self.multimodel_class.maxs);
};


void () multimodel_think =
{
	// called at the beginning of PlayerPostThink()
	if      (self.view_ofs == '0 0 0')       self.modelindex = 0;
	else if (self.classname == "ghost")	     self.modelindex = modelindex_eyes;
	else if (self.invisible_finished > time) self.modelindex = modelindex_eyes;
	else if (self.multimodel_class == world) self.modelindex = modelindex_eyes;
//	setting the model every frame causes bugs
//	else multimodel_setModel (self);
};

void () multimodel_touch =
{
	if (other.multimodel_class == self.multimodel_class) return;
	other.multimodel_class = self.multimodel_class;
	spawn_tfog (other.origin);
	multimodel_setModel (other);
	setsize (other, self.mins, self.maxs);
};

void() info_multimodel_class =
{
	self.classname = "multimodel_class";
	multimodel_classes = multimodel_classes + 1;
	precache_model (self.multimodel_class.model);

	// bounding box size
	if (self.multimodel_size == 0)
	{
		if (!self.mins) self.mins = VEC_HULL_MIN;
		if (!self.maxs) self.maxs = VEC_HULL_MAX;
	}
	if (self.multimodel_size == 1)
	{
		if (!self.mins) self.mins = VEC_HULL2_MIN;
		if (!self.maxs) self.maxs = VEC_HULL2_MAX;
	}
	if (self.height) self.maxs_z = self.mins_z + self.height;
};

void() misc_multimodel_selector =
{
	if (self.model)
	{
		self.multimodel_class = self;
		info_multimodel_class ();
	}
	else // default player model
	{
		local entity e;
		e = find (world, classname, "multimodel_class");
		while (e != world)
		{
			if (self.style == 0)       self.multimodel_class = e;
			if (self.style == e.style) self.multimodel_class = e;
			e = find (e, classname, "multimodel_class");
		}
		if (!self.multimodel_class) {remove (self); return;}
	}
	multimodel_setModel (self);
	setsize (self, '0 0 -24', '0 0 32'); // not too easy to hit
	self.touch = multimodel_touch;
	StartItem ();

	// set netname to "multimodel_selector" so PutClientInServer
	// can find out if there are any multimodel selectors
	self.netname = "multimodel_selector";
};

void() multimodel_cycleClasses =
{
	if (deathmatch) return; // no cheating
	self.multimodel_class = find (self.multimodel_class, classname, "multimodel_class");
	if (self.multimodel_class == world)
		self.multimodel_class = find (world, classname, "multimodel_class");
	setmodel (self, self.multimodel_class.model);
	self.skin = self.multimodel_class.skin;
	setsize  (self, self.multimodel_class.mins, self.multimodel_class.maxs);
};

void() multimodel_cycleSkins =
{
	self.skin = self.skin + 1;
	if (self.skin >= self.multimodel_class.multimodel_skins) self.skin = 0;
};
