/* Copyright (C) 1996-1997  Id Software, Inc.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

	See file, 'COPYING', for details.
*/

#include "client_changelevel.qc"
#include "client_spawn.qc"

// prototypes

void () W_WeaponFrame;
void () W_SetCurrentAmmo;
void () player_jump1;
void () player_die;
void () player_rise;
void () set_suicide_frame;  // called by ClientKill and DeadThink

float modelindex_eyes, modelindex_player;
.float dmgtime;

/*
============
ClientKill

Player entered the suicide command
============
*/
void() ClientKill = 
{
	bprint (PRINT_MEDIUM, self.netname);
	bprint (PRINT_MEDIUM, " suicides\n");
	self.rps_lastingDamage = self.rps_strength * self.rps_toughness;
	self.rps_superStrength = 0;
	player_die ();
#ifdef QUAKEWORLD
	logfrag (self, self);
#endif /* QUAKEWORLD */
	self.frags = self.frags - 2;
};

/*
=============================================================================

				ENTITIES

=============================================================================
*/

// The normal starting point for a level.
void() info_player_start = {};

// Only used on start map for the return point from an episode.
void() info_player_start2 = {};

// saved out by quaked in region mode
void() testplayerstart = {};

// potential spawning position for coop games
void() info_player_coop = {};

// potential spawning position for deathmatch games
void() monster_player;
void() info_player_deathmatch =
{
	local entity e;
	e = spawn ();
	setorigin (e, self.origin);
	e.angles    = self.angles;
	e.nextthink = time + 0.1;
	e.think     = monster_player;
	e.classname = "monster_player_deathmatch_start";
};

/*
===============================================================================

RULES

===============================================================================
*/

/*
go to the next level for deathmatch
if [not QUAKEWORLD], only called if a time or frag limit has expired
*/
void() NextLevel =
{
	local entity o;
#ifdef QUAKEWORLD
	local string newmap;
	if (nextmap != "") return; // already done
#endif /* QUAKEWORLD */

	if (mapname == "start")
	{
		if (!cvar("registered"))
		{
			mapname = "e1m1";
		}
		else if (!(serverflags & 1))
		{
			mapname = "e1m1";
			serverflags = serverflags | 1;
		}
		else if (!(serverflags & 2))
		{
			mapname = "e2m1";
			serverflags = serverflags | 2;
		}
		else if (!(serverflags & 4))
		{
			mapname = "e3m1";
			serverflags = serverflags | 4;
		}
		else if (!(serverflags & 8))
		{
			mapname = "e4m1";
			serverflags = serverflags - 7;
		}

		o = spawn();
		o.map = mapname;
	}
	else
	{
		// find a trigger changelevel
		o = find(world, classname, "trigger_changelevel");

		// go back to same map if no trigger_changelevel
		if (!o || mapname == "start")
		{
			o = spawn();
			o.map = mapname;
		}
	}

	nextmap = o.map;

#ifndef QUAKEWORLD
	gameover = TRUE;
#endif /* not QUAKEWORLD */
	
	if (o.nextthink < time)
	{
		o.think = execute_changelevel;
		o.nextthink = time + 0.1;
	}
};

/*
============
CheckRules

Exit deathmatch games upon conditions
============
*/
void() CheckRules =
{
#ifndef QUAKEWORLD
	local	float		timelimit;
	local	float		fraglimit;
	
	if (gameover) return; // someone else quit the game already

	timelimit = cvar("timelimit") * 60;
	fraglimit = cvar("fraglimit");
#endif /* not QUAKEWORLD */

	if (fraglimit && self.frags >= fraglimit) NextLevel ();
	else if (timelimit && time  >= timelimit) NextLevel ();
};

//============================================================================

void() PlayerDeathThink = 
{
	if (self.impulse == 97) camera_zoom_out ();
	if (self.impulse == 98) camera_zoom_in  ();
	if (rps_isDead (self) && self.classname != "ghost")
	{
		self.nextthink	= -1;
		CopyToBodyQue (self);
		self.deadflag	= DEAD_DEAD;
		self.movetype	= MOVETYPE_NOCLIP;
		self.health		= 999;
		self.classname  = "ghost";
		self.modelindex = modelindex_eyes;
		setorigin (self, self.camera.origin);
	}
	if ((!self.button0) && (!self.button2)) {self.deadflag = DEAD_RESPAWNABLE; return;}
	if (self.deadflag == DEAD_DEAD) return;
	if (self.classname == "ghost") {respawn (); return;}
	if (rps_isKnockedOut (self) == FALSE)
	{
		traceline (self.origin + '-16 -16 -24', self.origin + '-16 -16 32', FALSE, self);
		if (trace_fraction < 1) return;
		traceline (self.origin + '16 -16 -24',  self.origin + '16 -16 32',  FALSE, self);
		if (trace_fraction < 1) return;
		traceline (self.origin + '-16 16 -24',  self.origin + '-16 16 32',  FALSE, self);
		if (trace_fraction < 1) return;
		traceline (self.origin + '16 16 -24',   self.origin + '16 16 32',   FALSE, self);
		if (trace_fraction < 1) return;

		self.deadflag = DEAD_DYING;
		player_rise ();
	}
};

void() PlayerJump = 
{
	if (self.attack_finished > time) return;
	if (self.pain_finished > time && self.waterlevel == 0) return;
	if (self.flags & FL_WATERJUMP) return;
	if (self.waterlevel >= 2)
	{
		if		(self.watertype == CONTENT_WATER) self.velocity_z = 100;
		else if	(self.watertype == CONTENT_SLIME) self.velocity_z = 80;
		else                                      self.velocity_z = 10;
		self.rps_readyHitPoints = self.rps_readyHitPoints - (2 * frametime);
		if (self.swim_flag < time)
		{
			self.swim_flag = time + 1;
			if (random() < 0.5) sound (self, CHAN_BODY, "misc/water1.wav", 1, ATTN_NORM);
			else                sound (self, CHAN_BODY, "misc/water2.wav", 1, ATTN_NORM);
		}
		return;
	}
	if (!(self.flags & FL_ONGROUND))     return; // don't stairwalk
	if (!(self.flags & FL_JUMPRELEASED)) return; // don't pogo stick
	self.flags = self.flags - FL_ONGROUND;
	self.flags = self.flags - FL_JUMPRELEASED;
	
	makeplayervectors ();
	
	if (self.button2 && self.enemy != world) // jump around while fighting
	{
		local vector v;
		local float f;
		f = v_forward * self.velocity;
		if (f > -10) // moving sideways or forward
		{
			f = v_right * self.velocity;
			if (f > 0) v = normalize (v_forward + v_right); // 45 degrees right
			else       v = normalize (v_forward - v_right); // 45 degrees left
		}
		else // moving backward
		{
			v = (self.velocity - self.velocity_z * '0 0 1');
			if (v == '0 0 0') v = v_forward;
			else v = normalize (v);
		}
		self.velocity = '0 0 1' * self.velocity_z;
		self.velocity = self.velocity + (350 * v);
		self.velocity = self.velocity + '0 0 150';
		
		sound (self, CHAN_BODY, "player/plyrjmp8.wav", 1, ATTN_NORM);
		self.rps_readyHitPoints = self.rps_readyHitPoints - 1;
		player_jump1 ();
	}
	else if (self.button0) // running forward with attack button
	{
		self.velocity = '0 0 1' * self.velocity_z;
		self.velocity = self.velocity + (v_forward * 240);
		self.velocity = self.velocity + '0 0 270';
		sound (self, CHAN_BODY, "player/plyrjmp8.wav", 1, ATTN_NORM);
		self.rps_readyHitPoints = self.rps_readyHitPoints - 2;
		player_jump1 ();
	}
	else // regular jump
	{
		self.velocity = self.velocity + '0 0 270';
		sound (self, CHAN_BODY, "player/plyrjmp8.wav", 1, ATTN_NORM);
		self.rps_readyHitPoints = self.rps_readyHitPoints - 2;
		player_jump1 ();
	}

	self.button2 = 0;
};


void() WaterMove =
{
	if (self.movetype == MOVETYPE_NOCLIP) return;
	if (self.health < 0) return;

	// breathing
	if (self.waterlevel < 3)
	{
		if (self.air_finished < time)
			sound (self, CHAN_VOICE, "player/gasp2.wav", 1, ATTN_NORM);
		else if (self.air_finished < time + 9)
			sound (self, CHAN_VOICE, "player/gasp1.wav", 1, ATTN_NORM);
		self.air_finished = time + 30;
	}

	// getting out
	if (!self.waterlevel)
	{
		if (self.flags & FL_INWATER)
		{
			if (self.watertype == CONTENT_WATER) splash_spawn ();
			sound (self, CHAN_BODY, "misc/outwater.wav", 1, ATTN_NORM);
			self.flags = self.flags - FL_INWATER;
		}
		return;
	}

	// getting in
	if (!(self.flags & FL_INWATER))
	{	
		self.dmgtime = time + 0.3;
		self.flags = self.flags + FL_INWATER;
		if (self.watertype == CONTENT_LAVA)
			sound (self, CHAN_BODY, "player/inlava.wav", 1, ATTN_NORM);
		if (self.watertype == CONTENT_SLIME)
			sound (self, CHAN_BODY, "player/slimbrn2.wav", 1, ATTN_NORM);
		if (self.watertype == CONTENT_WATER)
		{
			sound (self, CHAN_BODY, "player/inh2o.wav", 1, ATTN_NORM);
			splash_spawn ();
		}
	}

	if (self.dmgtime < time)
	{
		// drowning, slime, or lava damage
		self.dmgtime = time + 1;
		if (self.watertype == CONTENT_WATER && self.air_finished < time)
			T_Damage (self, world, world, 5);
		if (self.watertype == CONTENT_SLIME && self.radsuit_finished < time)
			T_Damage (self, world, world, 5 * self.waterlevel);
		if (self.watertype == CONTENT_LAVA)
		{
			if (self.radsuit_finished < time) self.dmgtime = time + 0.2;
			T_Damage (self, world, world, 10 * self.waterlevel);
			local entity fire;
			fire = spawn ();
			setorigin (fire, self.origin + '0 0 8');
			setmodel  (fire, "progs/s_explod.spr");
			fire.frame     = 0;
			fire.think     = s_explode2;
			fire.nextthink = time + 0.1;
			fire.movetype  = MOVETYPE_NOCLIP;
			fire.velocity  = '0 0 100';
		}

		// not drowning, just splashing
		if (self.watertype == CONTENT_WATER && self.waterlevel < 3)
		{
			if (vlen (self.velocity) > 100)
			{
				self.dmgtime = time + 0.3;
				splash_spawn ();
				sound (self, CHAN_BODY, "player/inh2o.wav", 1, ATTN_NORM);
			}
		}
	}

	// viscosity
	if (!(self.flags & FL_WATERJUMP))
		self.velocity = self.velocity -
			(0.8 * self.waterlevel * frametime * self.velocity);
};

void (vector v) CheckSpace =
{
	v = v + self.origin;
	traceline (v, v + v_forward * 24, TRUE, self);
	if (trace_ent.classname == "func_ladder") trace_fraction = 1;
};

void() CheckWaterJump =
{
	local vector v;
	v = playerangles ();
	makevectors (v);

	// see if there is a ledge at jumping height	
	v = self.origin + '0 0 30';
	traceline (v, v + '0 0 32', TRUE, self);
	v = trace_endpos;
	traceline (v, v + v_forward * 24, TRUE, self);
	if (trace_fraction == 1)
	{
		v = self.origin + (v_forward * 24) + '0 0 20';
		traceline (trace_endpos, v, TRUE, self);
		if (trace_fraction < 1 && v_forward * self.velocity > 1)
			PlayerJump();
	}
	
	// look for empty space to jump into
	CheckSpace ('0 0 20');
	if (trace_fraction < 1)	CheckSpace ('0 0 10');
	if (trace_fraction < 1)	CheckSpace ('0 0 -1');
	if (trace_fraction < 1)	return;

	// if no ladder, try to find a surface to jump onto
	if (trace_ent.classname != "func_ladder")
	{
		v = self.origin + (v_forward * 24) + '0 0 -7';
		traceline (trace_endpos, v, TRUE, self);
		if (trace_fraction == 1) return;
	}
	
	// don't climb unless moving forward
	// just hang or use jump button to slide down
	if (self.velocity * v_forward <= 0 && self.button0 == 0)
	{
		if (self.button2 == 0)
			self.velocity_z = frametime * cvar ("sv_gravity");
		else if (self.velocity_z < -600)
			self.velocity_z = -600;
		return;
	}

	self.flags = self.flags | FL_WATERJUMP;
	self.flags = self.flags - (self.flags & (FL_ONGROUND | FL_JUMPRELEASED));
	self.rps_readyHitPoints = self.rps_readyHitPoints - (3 * frametime);
	self.velocity_z = (frametime * cvar ("sv_gravity")) + 270;
	self.teleport_time = time + 2;	// safety net
};

void() PlayerPreThink =
{
	local float f;
	
	if (intermission_running) IntermissionThink ();
	if (self.view_ofs == '0 0 0') return;

#ifdef QUAKEWORLD
	self.deathtype = "";
#endif /* QUAKEWORLD */

	CheckRules ();

	if (self.deadflag >= DEAD_DEAD) PlayerDeathThink ();
	if (self.deadflag >= DEAD_DYING) return;

	WaterMove ();
	CheckWaterJump (); // always climb - even when not in water

	if (self.button2) PlayerJump ();
	else self.flags = self.flags | FL_JUMPRELEASED;

	// teleporters can force a non-moving pause time	
	if (time < self.pausetime) self.velocity = '0 0 0';

	camera_preThink ();

	if (self.movetype != MOVETYPE_WALK) return;

	// Auto-Jump:
	if (self.waterlevel == 0)
	{
		f = vlen (self.velocity - self.velocity_z * '0 0 1');
		if (f > 120)
		{
			traceline (self.origin, self.origin + '0 0 -55', TRUE, self);
			if (trace_fraction == 1) PlayerJump();
		}
		else // don't fall off edge
		{
			local float tl, tr, bl, br, l, r, t, b, c;
			local vector o, p, v;
			o = self.origin;
			v = '0 0 -55';
			p = o + '0 0 0';     traceline (p, p + v, TRUE, self);  c  = trace_fraction == 1;
			p = o + '-32 0 0';   traceline (p, p + v, TRUE, self);  l  = trace_fraction == 1;
			p = o + '32 0 0';    traceline (p, p + v, TRUE, self);  r  = trace_fraction == 1;
			p = o + '0 32 0';    traceline (p, p + v, TRUE, self);  t  = trace_fraction == 1;
			p = o + '0 -32 0';   traceline (p, p + v, TRUE, self);  b  = trace_fraction == 1;
			p = o + '-32 32 0';  traceline (p, p + v, TRUE, self);  tl = trace_fraction == 1;
			p = o + '32 32 0';   traceline (p, p + v, TRUE, self);  tr = trace_fraction == 1;
			p = o + '-32 -32 0'; traceline (p, p + v, TRUE, self);  bl = trace_fraction == 1;
			p = o + '32 -32 0';  traceline (p, p + v, TRUE, self);  br = trace_fraction == 1;

/*
			sprint (self, PRINT_LOW, "\n");
			if (tl) sprint (self, PRINT_LOW, "   tl"); else sprint (self, PRINT_LOW, "     ");
			if (t)  sprint (self, PRINT_LOW, "  t  "); else sprint (self, PRINT_LOW, "     ");
			if (tr) sprint (self, PRINT_LOW, "tr \n"); else sprint (self, PRINT_LOW, "   \n");
			if (l)  sprint (self, PRINT_LOW, "   l "); else sprint (self, PRINT_LOW, "     ");
			if (c)  sprint (self, PRINT_LOW, "  c  "); else sprint (self, PRINT_LOW, "     ");
			if (r)  sprint (self, PRINT_LOW, " r \n"); else sprint (self, PRINT_LOW, "   \n");
			if (bl) sprint (self, PRINT_LOW, "   bl"); else sprint (self, PRINT_LOW, "     ");
			if (b)  sprint (self, PRINT_LOW, "  b  "); else sprint (self, PRINT_LOW, "     ");
			if (br) sprint (self, PRINT_LOW, "br \n"); else sprint (self, PRINT_LOW, "   \n");
*/

			if (c && !(tl && t && tr && l && c && r && bl && b && br))
			{
				if (t && b)
				{
					if (tl && l && bl && self.velocity_x < 0)
						self.velocity_x = 0;
					if (tr && r && br && self.velocity_x > 0)
						self.velocity_x = 0;
				}
				if (l && r)
				{
					if (tl && t && tr && self.velocity_y > 0)
						self.velocity_y = 0;
					if (bl && b && br && self.velocity_y < 0)
						self.velocity_y = 0;
				}
			}

/*
			if (c)
			{
				// nothin' but net
				if (tl && tr && bl && br) {}

				// standing on a corner
				else if (tr && bl && br)
				{
					if ((self.velocity_x > 0) && b) self.velocity_x = 0;
					if ((self.velocity_y < 0) && r) self.velocity_y = 0;
				}
				else if (tl && bl && br)
				{
					if ((self.velocity_x < 0) && b) self.velocity_x = 0;
					if ((self.velocity_y < 0) && l) self.velocity_y = 0;
				}
				else if (tl && tr && br)
				{
					if ((self.velocity_x > 0) && t) self.velocity_x = 0;
					if ((self.velocity_y > 0) && r) self.velocity_y = 0;
				}
				else if (tl && tr && bl)
				{
					if ((self.velocity_x < 0) && t) self.velocity_x = 0;
					if ((self.velocity_y > 0) && l) self.velocity_y = 0;
				}
				
				// standing on an edge
				else if (tl && tr)
					{if (self.velocity_y > 0) self.velocity_y = 0;}
				else if (bl && br)
					{if (self.velocity_y < 0) self.velocity_y = 0;}
				else if (tl && bl)
					{if (self.velocity_x < 0) self.velocity_x = 0;}
				else if (tr && br)
					{if (self.velocity_x > 0) self.velocity_x = 0;}

				// standing at a bend
				else if (tl)
				{
					if (self.velocity_x < 0 && self.velocity_y > 0)
						self.velocity = '0 0 1' * self.velocity_z;
				}
				else if (tr)
				{
					if (self.velocity_x > 0 && self.velocity_y > 0)
						self.velocity = '0 0 1' * self.velocity_z;
				}
				else if (bl)
				{
					if (self.velocity_x < 0 && self.velocity_y < 0)
						self.velocity = '0 0 1' * self.velocity_z;
				}
				else if (br)
				{
					if (self.velocity_x > 0 && self.velocity_y < 0)
						self.velocity = '0 0 1' * self.velocity_z;
				}
			}
*/
		}
	}

// powered turns only (only turn while moving forward or back)
if (cvar ("temp1"))
{
local float f, r;
makevectors ('0 1 0' * self.ideal_yaw);
f = v_forward * self.velocity;
r = v_right * self.velocity;
if (f < 0) f = -1 * f;
r = r / f;
if      (r >  0.1) r = r - 0.1;
else if (r < -0.1) r = r + 0.1;
else                r = 0;
self.velocity = self.velocity - v_right * (r * f);
}

	// counter the effect of downward camera angle on swimming
	if (self.waterlevel > 1 && self.button0 == 0)
	{
		local float sped, reaction;
		sped = vlen (self.velocity);
		makevectors (self.v_angle);
		reaction = v_forward_z * (v_forward * self.velocity);
		reaction = reaction * sped * frametime * 0.06;
		if (reaction / self.velocity_z > 1) self.velocity_z = 0;
		else self.velocity_z = self.velocity_z - reaction;
		self.velocity = normalize (self.velocity);
		self.velocity = self.velocity * sped;
	}

	// float
	f = cvar ("sv_gravity") * frametime;
	if (self.waterlevel == 2) self.velocity_z = self.velocity_z + f * 0.5;
	if (self.waterlevel == 3) self.velocity_z = self.velocity_z + f;
};

  ////////////////////////////////////////////////////
 // CheckPowerups - Check for turning off powerups //
////////////////////////////////////////////////////
void() CheckPowerups =
{
	if (self.health <= 0) return;

	// invisibility
	if (self.invisible_finished)
	{
		if (self.invisible_sound < time)
		{
			local float r;  r = random();
			if      (r < 0.33) sprint (self, PRINT_LOW, "You are SO invisible\n");
			else if (r < 0.67) sprint (self, PRINT_LOW, "Are you proud of yourself?\n");
			else               sprint (self, PRINT_LOW, "Look at me, I'm invisible\n");
			sound (self, CHAN_AUTO, "items/inv3.wav", 0.5, ATTN_IDLE);
			self.invisible_sound = time + ((random() * 3) + 1);
		}
		if (self.invisible_finished < time + 3)
		{
			if (self.invisible_time == 1)
			{
				sprint (self, PRINT_HIGH, "Invisibility is running out\n");
				sound (self, CHAN_AUTO, "items/inv2.wav", 1, ATTN_NORM);
			}
			if (self.invisible_time == 1 || self.invisible_time < time)
			{
				self.invisible_time = time + 1;
				stuffcmd (self, "bf\n");
			}
		}
		if (self.invisible_finished < time) // just ran out
		{
			sprint (self, PRINT_HIGH, "You are no longer invisible\n");
			self.items = self.items - (self.items & IT_INVISIBILITY);
			self.invisible_finished = 0;
			self.invisible_time = 0;
		}
	}

	// invulnerability
	if (self.invincible_finished)
	{
		if (self.invincible_finished < time + 3) // about to run out
		{
			if (self.invincible_time == 1)
			{
				sprint (self, PRINT_HIGH, "Protection has almost burned out\n");
				sound (self, CHAN_AUTO, "items/protect2.wav", 1, ATTN_NORM);
			}
			if (self.invincible_time == 1 || self.invincible_time < time)
			{
				self.invincible_time = time + 1;
				stuffcmd (self, "bf\n");
			}
		}
		if (self.invincible_finished < time)
		{
			sprint (self, PRINT_HIGH, "Protection is gone\n");
			self.items = self.items - (self.items & IT_INVULNERABILITY);
			self.invincible_time = 0;
			self.invincible_finished = 0;
		}
		if (self.invincible_finished > time)
		{
			self.effects = self.effects | EF_DIMLIGHT;
#ifdef QUAKEWORLD
			self.effects = self.effects | EF_RED;
#endif /* QUAKEWORLD */
		}
		else
		{
			self.effects = self.effects - (self.effects & EF_DIMLIGHT);
#ifdef QUAKEWORLD
			self.effects = self.effects - (self.effects & EF_RED);
#endif /* QUAKEWORLD */
		}
	}

	// quad damage
	if (self.super_damage_finished)
	{
		if (self.super_damage_finished < time + 3)
		{
			if (self.super_time == 1)
			{
				sprint (self, PRINT_HIGH, "Quad Damage is wearing off\n");
				sound (self, CHAN_AUTO, "items/damage2.wav", 1, ATTN_NORM);
			}	  
			if (self.super_time == 1 || self.super_time < time)
			{
				self.super_time = time + 1;
				stuffcmd (self, "bf\n");
			}
		}
		if (self.super_damage_finished < time)
		{
			sprint (self, PRINT_HIGH, "Quad Damage has worn off\n");
			self.items = self.items - (self.items & IT_QUAD);
			self.super_damage_finished = 0;
			self.super_time = 0;
		}
		if (self.super_damage_finished > time)
		{
			self.effects = self.effects | EF_DIMLIGHT;
#ifdef QUAKEWORLD
			self.effects = self.effects | EF_BLUE;
#endif /* QUAKEWORLD */
		}
		else
		{
			self.effects = self.effects - (self.effects & EF_DIMLIGHT);
#ifdef QUAKEWORLD
			self.effects = self.effects - (self.effects & EF_BLUE);
#endif /* QUAKEWORLD */
		}
	}	

	// suit	
	if (self.radsuit_finished)
	{
		self.air_finished = time + 30; // don't drown
		if (self.radsuit_finished < time + 3)
		{
			if (self.rad_time == 1)
			{
				sprint (self, PRINT_HIGH, "Air supply is running out\n");
				sound (self, CHAN_AUTO, "items/suit2.wav", 1, ATTN_NORM);
			}
			if (self.rad_time == 1 || self.rad_time < time)
			{
				self.rad_time = time + 1;
				stuffcmd (self, "bf\n");
			}
		}
		if (self.radsuit_finished < time)
		{
			sprint (self, PRINT_HIGH, "Air supply is used up\n");
			self.items = self.items - (self.items & IT_SUIT);
			self.rad_time = 0;
			self.radsuit_finished = 0;
		}
	}	
};

// check to see if player landed, play appropriate landing sound,
// keep track of player falling speed, and inflict damage for long falls
void() player_checkLanding =
{
	if (!(self.flags & FL_ONGROUND)) self.jump_flag = self.velocity_z;
	else if ((self.jump_flag < -300) && (self.health > 0))
	{
		if (self.watertype == CONTENT_WATER)
			sound (self, CHAN_BODY, "player/h2ojump.wav", 1, ATTN_NORM);
		else if (self.jump_flag > -650)
			sound (self, CHAN_VOICE, "player/land.wav", 1, ATTN_NORM);
		else
		{
			T_Damage (self, world, world, 5); 
			sound (self, CHAN_VOICE, "player/land2.wav", 1, ATTN_NORM);
			self.deathtype = "falling";
		}
		self.jump_flag = 0;
	}
};

void() PlayerPostThink = 
{
	player_rotation  ();
	player_tilt      ();
	camera_think     ();
	multimodel_think ();
	autotarg_think   ();

	if (self.view_ofs == '0 0 0') return; // intermission or finale
	if (self.deadflag) return;

	W_WeaponFrame ();
	CheckPowerups ();
	player_checkLanding();
	rps_think();

	if (self.modelindex == modelindex_eyes) self.frame = 0;
	if (self.watertype == CONTENT_LAVA) self.velocity = '0 0 0';
};

/*
===========
ClientConnect

called when a player connects to a server
============
*/
void() ClientConnect =
{
	bprint (PRINT_HIGH, self.netname);
	bprint (PRINT_HIGH, " entered the game\n");
	
	// a client connecting during an intermission can cause problems
	if (intermission_running)
#ifndef QUAKEWORLD
		ExitIntermission ();
#else /* QUAKEWORLD */
		GotoNextMap ();
#endif /* QUAKEWORLD */
};


/*
===========
ClientDisconnect

called when a player disconnects from a server
============
*/
void() ClientDisconnect =
{
#ifndef QUAKEWORLD
	if (gameover)
		return;
	// if the level end trigger has been activated, just return
	// since they aren't *really* leaving
#endif /* not QUAKEWORLD */

	// let everyone else know
	bprint (PRINT_HIGH, self.netname);
	bprint (PRINT_HIGH, " left the game with ");
	bprint (PRINT_HIGH, ftos(self.frags));
	bprint (PRINT_HIGH, " frags\n");
	sound (self, CHAN_BODY, "player/tornoff2.wav", 1, ATTN_NONE);
	set_suicide_frame ();
};

void (entity attacker, entity targ) ClientFragCount =
{
	if (attacker == targ) attacker.frags = attacker.frags - 1;
	else attacker.frags = attacker.frags + 1;
#ifdef QUAKEWORLD
	logfrag (attacker, targ);
#endif /* QUAKEWORLD */
};

void (entity targ, entity attacker) ClientObituary = 
{
	local float rnum;
	rnum = random();

	if ((targ.flags & (FL_MONSTER|FL_CLIENT)) == 0) return;

	if (!attacker.netname && attacker.flags & FL_MONSTER) attacker.netname = "a monster";
	if (!targ.netname     && targ.flags     & FL_MONSTER) targ.netname     = "a monster";
	
	if (attacker.classname == "teledeath")
	{
		if (attacker.owner == self)
		{
			bprint (PRINT_MEDIUM, targ.netname);
			bprint (PRINT_MEDIUM, " telefragged himself\n");
			return;
		}
		bprint (PRINT_MEDIUM, targ.netname);
		bprint (PRINT_MEDIUM, " was telefragged by ");
		bprint (PRINT_MEDIUM, attacker.owner.netname);
		bprint (PRINT_MEDIUM, "\n");

		ClientFragCount (attacker.owner, targ);
	}
	else if (attacker.classname == "teledeath2")
	{
		bprint (PRINT_MEDIUM, targ.netname);
		bprint (PRINT_MEDIUM, " could not overcome ");
		bprint (PRINT_MEDIUM, attacker.owner.netname);
		bprint (PRINT_MEDIUM, "'s structural integrity\n");

		ClientFragCount (targ, targ);
	}
	else if ((targ.flags & FL_MONSTER)
		|| (attacker.classname == "player")
		|| (attacker.classname == "monster_player")
		|| (attacker.classname == "monster_player_deathmatch_start"))
	{
		if (!attacker)
		{
			// a monster simply died
			bprint (PRINT_MEDIUM, targ.netname);
			bprint (PRINT_MEDIUM, " died\n");
		}
#ifdef QUAKEWORLD
		else if ((teamplay == 2) && (infokey(targ,"team") == "")
			&& (infokey(targ,"team") == infokey(attacker,"team")))
#else /* if not QUAKEWORLD */
		else if ((teamplay == 2) && (targ.team) && (targ.team == attacker.team))
#endif /* not QUAKEWORLD */
		{
			// killed by a teammate
			bprint (PRINT_MEDIUM, attacker.netname);
			if      (rnum < 0.25) bprint(PRINT_MEDIUM, " whacks a teammate\n");
			else if (rnum < 0.50) bprint(PRINT_MEDIUM, " checks his glasses\n");
			else if (rnum < 0.75) bprint(PRINT_MEDIUM, " scores for the other team\n");
			else                  bprint(PRINT_MEDIUM, " loses another friend\n");

			ClientFragCount (attacker, attacker);
		}
		else
		{
			// killed by a player or NPC
			bprint (PRINT_MEDIUM, attacker.netname);
			if      (rnum < 0.25) bprint (PRINT_MEDIUM, " slew ");
			else if (rnum < 0.50) bprint (PRINT_MEDIUM, " finished ");
			else if (rnum < 0.75) bprint (PRINT_MEDIUM, " terminated ");
			else                  bprint (PRINT_MEDIUM, " brought about the end of ");
			bprint (PRINT_MEDIUM, targ.netname);
			if      (rnum < 0.25) bprint (PRINT_MEDIUM, "\n");
			else if (rnum < 0.50) bprint (PRINT_MEDIUM, " off\n");
			else if (rnum < 0.75) bprint (PRINT_MEDIUM, "'s command\n");
			else                  bprint (PRINT_MEDIUM, "\n");

			ClientFragCount (attacker, targ);
		}
	}
	else if (attacker.flags & FL_MONSTER)
	{
		// killed by a monster
		bprint (PRINT_MEDIUM, targ.netname);
		if (attacker.classname == "monster_army")        bprint (PRINT_MEDIUM, " was shot by a Grunt\n");
		if (attacker.classname == "monster_demon1")      bprint (PRINT_MEDIUM, " was eviscerated by a Fiend\n");
		if (attacker.classname == "monster_dog")         bprint (PRINT_MEDIUM, " was mauled by a Rottweiler\n");
		if (attacker.classname == "monster_dragon")      bprint (PRINT_MEDIUM, " was fried by a Dragon\n");
		if (attacker.classname == "monster_enforcer")    bprint (PRINT_MEDIUM, " was blasted by an Enforcer\n");
		if (attacker.classname == "monster_fish")        bprint (PRINT_MEDIUM, " was fed to the Rotfish\n");
		if (attacker.classname == "monster_hell_knight") bprint (PRINT_MEDIUM, " was slain by a Death Knight\n");
		if (attacker.classname == "monster_knight")      bprint (PRINT_MEDIUM, " was slashed by a Knight\n");
		if (attacker.classname == "monster_ogre")        bprint (PRINT_MEDIUM, " was destroyed by an Ogre\n");
		if (attacker.classname == "monster_oldone")      bprint (PRINT_MEDIUM, " became one with Shub-Niggurath\n");
		if (attacker.classname == "monster_shalrath")    bprint (PRINT_MEDIUM, " was exploded by a Vore\n");
		if (attacker.classname == "monster_shambler")    bprint (PRINT_MEDIUM, " was smashed by a Shambler\n");
		if (attacker.classname == "monster_tarbaby")     bprint (PRINT_MEDIUM, " was slimed by a Spawn\n");
		if (attacker.classname == "monster_vomit")       bprint (PRINT_MEDIUM, " was vomited on by a Vomitus\n");
		if (attacker.classname == "monster_wizard")      bprint (PRINT_MEDIUM, " was scragged by a Scrag\n");
		if (attacker.classname == "monster_zombie")      bprint (PRINT_MEDIUM, " joins the Zombies\n");

		ClientFragCount (targ, targ);
	}
	else
	{
		// tricks and traps
		bprint (PRINT_MEDIUM, targ.netname);
		if (attacker != world && attacker.solid == SOLID_BSP) bprint (PRINT_MEDIUM, " was squished\n");
		else if (attacker.classname == "trigger_changelevel") bprint (PRINT_MEDIUM, " tried to leave\n");
		else if (attacker.classname == "trap_spikeshooter")   bprint (PRINT_MEDIUM, " was spiked\n");
		else if (attacker.classname == "trap_shooter")        bprint (PRINT_MEDIUM, " was spiked\n");
		else if (attacker.classname == "explo_box")           bprint (PRINT_MEDIUM, " blew up\n");
		else if (attacker.classname == "fireball")            bprint (PRINT_MEDIUM, " ate a lavaball\n");
		else if (targ.watertype == CONTENT_WATER)             bprint (PRINT_MEDIUM, " sleeps with the fishes\n");
		else if (targ.watertype == CONTENT_SLIME)             bprint (PRINT_MEDIUM, " gulped a load of slime\n");
		else if (targ.watertype == CONTENT_LAVA)              bprint (PRINT_MEDIUM, " burst into flames\n");
		else if (targ.deathtype == "falling")                 bprint (PRINT_MEDIUM, " fell to his death\n");
		else                                                  bprint (PRINT_MEDIUM, " died\n");
		targ.deathtype = "";

		ClientFragCount (targ, targ);
	}
};
