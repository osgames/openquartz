/* Copyright (C) 1996-1997  Id Software, Inc.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

	See file, 'COPYING', for details.
*/

void() SUB_regen =
{
	self.model = self.mdl;		// restore original model
	self.solid = SOLID_TRIGGER;	// allow it to be touched again
	sound (self, CHAN_VOICE, "items/itembk2.wav", 1, ATTN_NORM);
	setorigin (self, self.origin);
};

// prints a warning message when spawned
void() noclass =
{
	dprint ("noclass spawned at");
	dprint (vtos(self.origin));
	dprint ("\n");
	remove (self);
};

// plants the object on the floor
void() PlaceItem =
{
	self.mdl      = self.model;		// so it can be restored on respawn
	self.flags    = FL_ITEM;		// make extra wide
	self.solid    = SOLID_TRIGGER;
	self.movetype = MOVETYPE_TOSS;	
	self.velocity = '0 0 0';
	self.origin_z = self.origin_z + 6;
	if (!droptofloor())
	{
		dprint ("Bonus item fell out of level at ");
		dprint (vtos(self.origin));
		dprint ("\n");
		remove(self);
		return;
	}
};

// Sets the clipping size and plants the object on the floor
void() StartItem =
{
	self.nextthink = time + 0.2; // items start after other solids
	self.think     = PlaceItem;
};

// armor and health respawn time
void() set_respawn_time =
{
	if (deathmatch == 3) self.nextthink = time + 20;
	if (deathmatch == 1) self.nextthink = time + 90 + random() * 60;
	if (coop == 3) self.nextthink = time + 20;
	if (coop == 1) self.nextthink = time + 90 + random() * 60;
	self.think = SUB_regen;
};

// no weapons
void() weapon_supershotgun		= {remove(self);};
void() weapon_nailgun			= {remove(self);};
void() weapon_supernailgun		= {remove(self);};
void() weapon_grenadelauncher	= {remove(self);};
void() weapon_rocketlauncher	= {remove(self);};
void() weapon_lightning			= {remove(self);};

// no ammo
void() item_shells	= {remove(self);};
void() item_spikes	= {remove(self);};
void() item_rockets	= {remove(self);};
void() item_cells	= {remove(self);};
void() item_weapon	= {remove(self);};

// no backpacks
void() DropBackpack = {};

#include "item_armor.qc"	// armor
#include "item_artifact.qc"	// power-ups
#include "item_health.qc"	// health
#include "item_key.qc"		// keys and keycards
#include "item_sigil.qc"	// episode runes


#ifdef QUAKEWORLD

void() q_touch =
{
	local string s;

	if (other.classname != "player" || other.health <= 0) return;

	other.super_time = 1;
	other.super_damage_finished = self.cnt;
	other.items = other.items | IT_QUAD;

	s = ftos (rint (other.super_damage_finished - time));

	bprint (PRINT_LOW, other.netname);
	bprint (PRINT_LOW, " recovered a Quad with ");
	bprint (PRINT_LOW, s);
	bprint (PRINT_LOW, " seconds remaining!\n");

	sound (other, CHAN_VOICE, self.noise, 1, ATTN_NORM);
	stuffcmd (other, "bf\n");

	self.mdl = self.model;
	self.solid = SOLID_NOT;
	self.model = string_null;

	activator = other;
	SUB_UseTargets ();
};


void(float timeleft) DropQuad =
{
	local entity item;

	item = spawn();
	item.origin = self.origin;
	
	item.velocity_z = 300;
	item.velocity_x = -100 + (random() * 200);
	item.velocity_y = -100 + (random() * 200);
	
	item.flags = FL_ITEM;
	item.solid = SOLID_TRIGGER;
	item.movetype = MOVETYPE_TOSS;
	item.noise = "items/damage.wav";
	setmodel (item, "progs/quaddama.mdl");
	setsize (item, '-16 -16 -24', '16 16 32');
	item.cnt = time + timeleft;
	item.touch = q_touch;
	item.nextthink = time + timeleft;    // remove it with the time left on it
	item.think = SUB_Remove;
};


void() r_touch =
{
	local string s;

	if (other.classname != "player" || other.health <= 0) return;

	other.invisible_time = 1;
	other.invisible_finished = self.cnt;
	other.items = other.items | IT_INVISIBILITY;

	s = ftos (rint (other.invisible_finished - time));

	bprint (PRINT_LOW, other.netname);
	bprint (PRINT_LOW, " recovered a Ring with ");
	bprint (PRINT_LOW, s);
	bprint (PRINT_LOW, " seconds remaining!\n");

	sound (other, CHAN_VOICE, self.noise, 1, ATTN_NORM);
	stuffcmd (other, "bf\n");

	self.mdl = self.model;
	self.solid = SOLID_NOT;
	self.model = string_null;

	activator = other;
	SUB_UseTargets ();
};


void(float timeleft) DropRing =
{
	local entity    item;

	item = spawn();
	item.origin = self.origin;
	
	item.velocity_z = 300;
	item.velocity_x = -100 + (random() * 200);
	item.velocity_y = -100 + (random() * 200);
	
	item.flags = FL_ITEM;
	item.solid = SOLID_TRIGGER;
	item.movetype = MOVETYPE_TOSS;
	item.noise = "items/inv1.wav";
	setmodel (item, "progs/invisibl.mdl");
	setsize (item, '-16 -16 -24', '16 16 32');
	item.cnt = time + timeleft;
	item.touch = r_touch;
	item.nextthink = time + timeleft;    // remove after 30 seconds
	item.think = SUB_Remove;
};

#endif /* QUAKEWORLD */
