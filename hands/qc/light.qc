/* Copyright (C) 1996-1997  Id Software, Inc.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

	See file, 'COPYING', for details.
*/

/*
// light source - with enhanced customization options
class misc_light
{
	void() light;       // sets light style and starts ambient sound
	void() light_use;   // toggle light on and off

	int style           // there is a limit of 31 custom styles from 1..31
	                    // styles 1..12 are defined in worldspawn
	                    // so the 19 styles from 13..31 are up for grabs
	                    // if style == 0 a style number will be assigned automatically
	                    // but this is useful for multiple lights of the same style

    string message;     // the pattern of this custom light style
                        // a string of up to 64 characters in the a..z range
                        // example: "aabbcdfhlprtuvwwxxxxwwvutrplhfdcbbaa"

	float volume;       // ambient sound volume on a 0..1 scale

	string noise;       // name of the .wav sound file to play
	                    // path is relative to the game/sound directory
	                    // "ambience/mymusic.wav" for example

	float attenuation;  // 0 = no attenuation (default)
	                    // 1 = normal attenuation
	                    // 2 = "idle" attenuation
	                    // 3 = "static" attenuation
};
*/

float light_styles;

.float volume;
.float attenuation;

float START_OFF = 1;

void() light_use =
{
	if (self.spawnflags & START_OFF)
	{
		lightstyle(self.style, "m");
		self.spawnflags = self.spawnflags - START_OFF;
	}
	else
	{
		lightstyle(self.style, "a");
		self.spawnflags = self.spawnflags + START_OFF;
	}
};

void (string snd, string modl) light_init =
{
	if (!self.model)   self.model   = modl;
	if (!self.noise)   self.noise   = snd;

	precache_sound (self.noise);
	precache_model (self.model);
	setmodel (self, self.model);

	if (self.angles_y == -1) self.angles = '90 0 0';
	if (self.angles_y == -2) self.angles = '-90 0 0';
	if (self.mangle)         self.angles = self.mangle;
};

void() light =
{
	if (self.noise) // ambient sound
	{
		if (!self.volume) self.volume = 1;
		precache_sound (self.noise);
		ambientsound (self.origin, self.noise, self.volume, self.attenuation);
	}
	if (self.targetname != "" && self.style >= 32) // switchable
	{
		self.use = light_use;
		if (self.spawnflags & START_OFF) lightstyle (self.style, "a");
		else                             lightstyle (self.style, "m");
	}
	else // not switchable
	{
		if (self.message) // custom lightstyle
		{
			if (!self.style) // automatically assign style number
			{
				if (!light_styles) light_styles = 12; // style 0..11 defined in worldspawn
				self.style   = light_styles;
				light_styles = light_styles + 1;
				if (light_styles > 31) dprint ("Too many custom light styles!\n");
			}
			lightstyle (self.style, self.message);
		}
		remove (self);
	}
};

/*QUAKED light_fluoro (0 1 0) (-8 -8 -8) (8 8 8) START_OFF
Non-displayed light.
Default light value is 300
Default style is 0
If targeted, it will toggle between on or off.
Makes steady fluorescent humming sound
*/
void() light_fluoro =
{
	if (self.style >= 32)
	{
		self.use = light_use;
		if (self.spawnflags & START_OFF)
			lightstyle(self.style, "a");
		else
			lightstyle(self.style, "m");
	}
	
	precache_sound ("ambience/fl_hum1.wav");
	ambientsound (self.origin, "ambience/fl_hum1.wav", 0.5, ATTN_STATIC);
};

/*QUAKED light_fluorospark (0 1 0) (-8 -8 -8) (8 8 8)
Non-displayed light.
Default light value is 300
Default style is 10
Makes sparking, broken fluorescent sound
*/
void() light_fluorospark =
{
	if (!self.style)
		self.style = 10;

	precache_sound ("ambience/buzz1.wav");
	ambientsound (self.origin, "ambience/buzz1.wav", 0.5, ATTN_STATIC);
};

/*QUAKED light_globe (0 1 0) (-8 -8 -8) (8 8 8)
Sphere globe light.
Default light value is 300
Default style is 0
*/
void() light_globe =
{
	precache_model ("progs/s_light.spr");
	setmodel (self, "progs/s_light.spr");
	makestatic (self);
};

void() FireAmbient =
{
	precache_sound ("ambience/fire1.wav");
	// attenuate fast
	ambientsound (self.origin, "ambience/fire1.wav", 0.5, ATTN_STATIC);
};

void() light_torch_small_walltorch =
{
	precache_model ("progs/flame.mdl");
	setmodel (self, "progs/flame.mdl");
	FireAmbient ();
	makestatic (self);
};

void() light_flame_large_yellow =
{
	precache_model ("progs/flame2.mdl");
	setmodel (self, "progs/flame2.mdl");
	self.frame = 1;
	FireAmbient ();
	makestatic (self);
};

void() light_flame_small_yellow =
{
	precache_model ("progs/flame2.mdl");
	setmodel (self, "progs/flame2.mdl");
	FireAmbient ();
	makestatic (self);
};

void() light_flame_small_white =
{
	precache_model ("progs/flame2.mdl");
	setmodel (self, "progs/flame2.mdl");
	FireAmbient ();
	makestatic (self);
};

