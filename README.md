# Open Quartz

Originally developed at https://sourceforge.net/projects/openquartz/ by [mapes](http://sourceforge.net/users/mapes), [rsmd](http://sourceforge.net/users/rsmd) and [serplord](http://sourceforge.net/users/serplord) and published under the GPL-2.0 license.